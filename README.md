# Comandos

* download image hello-word e/ou executar container da image: ``docker container run hello-world``

* download image debian e/ou executar container debian e cmdo no container: ``docker container debian bash --version``

* processos em execução: ``docker container ps``

* processos já executados: ``docker container ps -a``

* Flag -i (para modo iterativo) -t (acesso ao terminal): ``docker container run -it debian bash``

* Help: ``docker container --help`` ``docker image run --help`` ``docker volume --help``

* atribuir name ao container: ``docker container run --name mydeb -it debian bash``

* listar containers: ``docker container ls`` ``docker container list`` ``docker container ps`` com ou sem flag ``-a``

* iniciar container pelo nome "mydeb": ``docker container start -ai mydeb``

* iniciar processo de container em uma porta específica: ``docker container run -p 8080:80 nginx``

* mapear volume: ``docker container run -p 8080:80 -v $(pwd)/not-found:/usr/share/nginx/html nginx``

* mapear volume 2: ``docker container run -p 8080:80 -v $(pwd)/html:/usr/share/nginx/html nginx``

* em background: ``docker container run -d --name ex-daemon-basic -p 8080:80 -v $(pwd)/html:/usr/share/nginx/html nginx`` 

* parar container: ``docker container stop ex-daemon-basic``

* start container: ``docker container start ex-daemon-basic``

* restart container: ``docker container restart ex-daemon-basic``

* ver logs: ``docker container logs ex-daemon-basic2``

* inspecionar:  ``docker container inspect ex-daemon-basic2``

* ver versão do container: ``docker container exec ex-daemon-basic2 uname -or``

* listar images apagar image: ``docker image ls`` ``docker image rm``

* listar volume: ``docker volume ls``

* pull image docker: ``docker image pull redis:latest``

* mudar tag image: ``docker image tag redis:latest ffcosta-redis``

* remover image: ``docker image rm redis:latest ffcosta-redis``

* build image dentro do diretorio do arquivo Dockerfile: ``docker image build -t ex-simple-build .``

* iniciar container do build: ``docker container run -p 80:80 ex-simple-build``

* argumento container build: ``docker container run ex-build-arg bash -c 'echo $S3_BUCKET'``

* alterando os args: ``docker image build --build-arg S3_BUCKET=myapp -t ex-build-arg .``

* filtros no inspect: ``docker image inspect --format="{{index .Config.Labels \"maintainer\"}}" ex-build-arg``

* build 1 : ``docker image build -t ex-build-copy .``

* run: ``docker container run -p 80:80 ex-build-copy``

* build: ``docker image build -t ex-build-dev .``

* run: ``docker container run -it -v $(pwd):/app -p 80:8000 --name python-server ex-build-dev``

* exibir conteúdo de volume: ``docker container run -it --volumes-from=python-server debian cat /log/http-server.log``

* redes: ``docker container run -d --net none debian``

* redes1: ``docker container run --rm alpine ash -c "ifconfig"``

* redes2: ``docker container run --rm --net none alpine ash -c "ifconfig"``

* rede3: ``docker container run -d --name container1 alpine sleep 1000``  ``docker container exec -it container1 ifconfig``

* rede4: ``docker network create --driver bridge rede_nova``

* rede5: ``docker network connect bridge container3``

* rede6: ``docker network disconnect bridge container3``

* rede7: ``docker container run -d --name container4 --net host alpine sleep 1000``


## Projeto Final

1. email1: ``docker-compose up -d``

2. email2: ``docker-compose ps``

3. email3: ``docker-compose exec db psql -U postgres -c '\l'``

4. email4 - parar serviços: ``docker-compose down``

5. email5: ``docker-compose exec db psql -U postgres -f /scripts/check.sql``

6. email6 logs: ``docker-compose logs -f -t``

7. email7 consultar db: ``docker-compose exec db psql -U postgres -d email_sender -c 'select * from emails'``

8. email1: ``docker-compose up -d --scale worker=3``